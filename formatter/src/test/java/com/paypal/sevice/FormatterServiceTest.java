package com.paypal.sevice;

import com.paypal.domain.FeedConcept;
import com.paypal.enums.NewsReaderObjectType;
import com.paypal.service.FormatterService;
import com.paypal.service.FormatterServiceImpl;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class FormatterServiceTest {
    private FormatterService formatterService;

    @Before
    public void setUp(){
        formatterService = new FormatterServiceImpl();
    }

    @Test
    public void checkWithFeedConcept() {
        String s = "Obama visited Facebook headquarters: http://bit.ly/xyz @elversatile";
        FeedConcept feedConcept = new FeedConcept();
        feedConcept.setStartPosition(14);
        feedConcept.setEndPosition(22);
        feedConcept.setNewsReaderObjectType(NewsReaderObjectType.ENTITY);
        List<FeedConcept> feedConcepts = new ArrayList<FeedConcept>();
        feedConcepts.add(feedConcept);

        FeedConcept feedConcept1 = new FeedConcept();
        feedConcept1.setStartPosition(0);
        feedConcept1.setEndPosition(5);
        feedConcept1.setNewsReaderObjectType(NewsReaderObjectType.ENTITY);
        feedConcepts.add(feedConcept1);

        FeedConcept feedConcept2 = new FeedConcept();
        feedConcept2.setStartPosition(56);
        feedConcept2.setEndPosition(67);
        feedConcept2.setNewsReaderObjectType(NewsReaderObjectType.USER_NAME);
        feedConcepts.add(feedConcept2);


        FeedConcept feedConcept3 = new FeedConcept();
        feedConcept3.setStartPosition(137);
        feedConcept3.setEndPosition(154);
        feedConcept3.setNewsReaderObjectType(NewsReaderObjectType.LINK);
        feedConcepts.add(feedConcept3);

        String result = formatterService.formatNewsFeed(s,feedConcepts);

        assertEquals(result,"<strong>Obama</strong> visited <strong>Facebook</strong> headquarters: http://bit.ly/xyz @<a href=\"http://twitter.com/elversatile\">elversatile</a>");
    }

    @Test
    public void checkWithoutFeedConcept() {
        String s = "Obama visited Facebook headquarters: http://bit.ly/xyz @elversatile";
        String result = formatterService.formatNewsFeed(s,null);
        assertEquals(result,s);
    }


}
