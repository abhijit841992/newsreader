package com.paypal.rules.impl;

import com.paypal.rules.Rule;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class LinksRuleTest {
    private Rule linkRule;

    @Before
    public void setUp(){
        linkRule = new LinksRule();
    }

    @Test
    public void checkWithHrefLinkRule() {
        String s = "http://paypal.com/";
        String result = linkRule.apply(s);
        assertEquals(result,"<a href=\""+s+"\">"+s+"</a>");
    }

    @Test
    public void checkWithOutHrefLinkRule() {
        String s = "http://paypal.com/";
        String result = linkRule.apply(s);
        assertNotEquals(result,"<a href=\""+s+">"+s+"</a>");
    }
}
