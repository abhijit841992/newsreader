package com.paypal.rules.impl;

import com.paypal.rules.Rule;
import org.junit.Before;
import org.junit.Test;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class EntityRuleTest {
    private Rule entityRule;

    @Before
    public void setUp(){
        entityRule = new EntityRule();
    }

    @Test
    public void checkWithStrongTagEntityRule() {
       String s = "Obama";
       String result = entityRule.apply(s);
       assertEquals(result,"<strong>"+s+"</strong>");
    }

    @Test
    public void checkWithOutStrongTagEntityRule() {
        String s = "Obama";
        String result = entityRule.apply(s);
        assertNotEquals(result,"<link>"+s+"</link>");
    }
}
