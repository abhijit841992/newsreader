package com.paypal.rules.impl;

import com.paypal.rules.Rule;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class UserNameRuleTest {
    private Rule userNameRule;

    @Before
    public void setUp(){
        userNameRule = new UserNameRule();
    }

    @Test
    public void checkWithHref() {
        String s = "elversatile";
        String result = userNameRule.apply(s);
        assertEquals(result,"<a href=\"http://twitter.com/"+s+"\">"+s+"</a>");
    }

    @Test
    public void checkWithOutHref() {
        String s = "elversatile";
        String result = userNameRule.apply(s);
        assertNotEquals(result,"<a href=\"http://twitter.com"+s+"\">"+s+"</a>");
    }
}
