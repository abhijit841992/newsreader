package com.paypal.rules.impl;

import com.paypal.rules.Rule;
import com.paypal.utils.NewsReaderConstant;

public class EntityRule implements Rule {

    public String apply(String entity) {
        if (entity == null || "".equalsIgnoreCase(entity)) return "";
        StringBuilder result = new StringBuilder();
        result.append(NewsReaderConstant.Formatter.OPEN_STRONG_TAG).append(entity).append(NewsReaderConstant.Formatter.CLOSE_STRONG_TAG);
        return result.toString();
    }

}
