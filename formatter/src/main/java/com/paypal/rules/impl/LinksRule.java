package com.paypal.rules.impl;

import com.paypal.rules.Rule;
import com.paypal.utils.NewsReaderConstant;

public class LinksRule implements Rule {

    public String apply(String link) {
        if (link == null || "".equalsIgnoreCase(link)) return "";
        StringBuilder result = new StringBuilder();
        String s = String.format(NewsReaderConstant.Formatter.OPEN_A_HREF, link);
        result.append(s).append(link).append(NewsReaderConstant.Formatter.CLOSE_A_HREF);
        return result.toString();
    }
}
