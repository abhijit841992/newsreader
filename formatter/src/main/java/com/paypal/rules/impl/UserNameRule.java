package com.paypal.rules.impl;

import com.paypal.rules.Rule;
import com.paypal.utils.NewsReaderConstant;

public class UserNameRule implements Rule {

    public String apply(String userName) {
        if (userName == null || "".equalsIgnoreCase(userName)) return "";
        StringBuilder result = new StringBuilder();
        String s = String.format(NewsReaderConstant.Formatter.OPEN_A_HREF, NewsReaderConstant.Formatter.TWITTER_URL + userName);
        result.append(s).append(userName).append(NewsReaderConstant.Formatter.CLOSE_A_HREF);
        return result.toString();
    }
}
