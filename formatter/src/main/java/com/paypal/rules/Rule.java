package com.paypal.rules;

public interface Rule {
    public String apply(String s);
}
