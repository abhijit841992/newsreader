package com.paypal.service;

import com.paypal.domain.FeedConcept;
import com.paypal.rules.Rule;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class FormatterServiceImpl implements FormatterService {

    private RuleFactory ruleFactory;

    public FormatterServiceImpl() {
        ruleFactory = new RuleFactory();
    }

    private Comparator<FeedConcept> getComparator() {
        return new Comparator<FeedConcept>() {
            public int compare(FeedConcept first, FeedConcept second) {
                return first.getStartPosition() - second.getStartPosition();
            }
        };
    }

    public String formatNewsFeed(String actualString, List<FeedConcept> feedConcepts) {
        if (actualString == null || "".equalsIgnoreCase(actualString)) return actualString;
        if (feedConcepts != null && !feedConcepts.isEmpty())
            Collections.sort(feedConcepts, getComparator());
        else
            return actualString;
        int len = actualString.length();
        int start = 0;
        int index = 0;
        int feedConceptSize = feedConcepts.size();
        StringBuilder result = new StringBuilder();
        while (start < len) {
            if (index >= feedConceptSize) {
                result.append(actualString.substring(start, len));
                start = len;
            } else {
                FeedConcept feedConcept = feedConcepts.get(index);
                if (feedConcept.getStartPosition() > start) {
                    result.append(actualString.substring(start, feedConcept.getStartPosition()));
                    start = feedConcept.getStartPosition();
                } else {
                    Rule rule = ruleFactory.getRule(feedConcept.getNewsReaderObjectType());
                    String formattedString = rule.apply(actualString.substring(feedConcept.getStartPosition(), feedConcept.getEndPosition()));
                    result.append(formattedString);
                    start = feedConcept.getEndPosition();
                    index = index + 1;
                }
            }
        }
        return result.toString();
    }


}
