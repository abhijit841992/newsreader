package com.paypal.service;

import com.paypal.enums.NewsReaderObjectType;
import com.paypal.rules.Rule;
import com.paypal.rules.impl.EntityRule;
import com.paypal.rules.impl.LinksRule;
import com.paypal.rules.impl.UserNameRule;

public class RuleFactory {
    private final Rule linkRule;
    private final Rule entityRule;
    private final Rule userNameRule;

    public RuleFactory(){
      linkRule = new LinksRule();
      entityRule = new EntityRule();
      userNameRule = new UserNameRule();
    }

    public  Rule getRule(NewsReaderObjectType type){
        switch (type){
            case LINK:
                return linkRule;
            case ENTITY:
                return entityRule;
            case USER_NAME:
                return userNameRule;
            default:
                return null;

        }
    }
}
