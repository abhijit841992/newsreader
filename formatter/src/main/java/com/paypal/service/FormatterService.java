package com.paypal.service;

import com.paypal.domain.FeedConcept;

import java.util.List;

public interface FormatterService {
    public String formatNewsFeed(String actualString, List<FeedConcept> feedConcepts);
}
