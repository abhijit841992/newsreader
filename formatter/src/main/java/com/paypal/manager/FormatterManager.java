package com.paypal.manager;

import com.paypal.domain.FeedConcept;
import com.paypal.domain.GenericResponse;
import com.paypal.domain.Status;
import com.paypal.service.FormatterService;
import com.paypal.service.FormatterServiceImpl;

import java.util.List;

public class FormatterManager {
    private FormatterService formatterService;

    public FormatterManager() {
        formatterService = new FormatterServiceImpl();
    }

    public GenericResponse<String> formatNewsFeed(String actualString, List<FeedConcept> feedConcepts) {
        String result = formatterService.formatNewsFeed(actualString, feedConcepts);
        GenericResponse.GenericResponseBuilder genericResponseBuilder = GenericResponse.<String>builder();
        if (result == null) {
            genericResponseBuilder.status(Status.builder().statusMessage("Error occurred while processing ").build());
        } else {
            genericResponseBuilder.data(result).status(Status.builder().statusMessage("SuccessFully formatted").build());
        }
        return genericResponseBuilder.build();

    }

}
