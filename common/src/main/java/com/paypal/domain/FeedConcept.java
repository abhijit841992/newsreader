package com.paypal.domain;

import com.paypal.enums.NewsReaderObjectType;

public class FeedConcept {
    private int startPosition;
    private int endPosition;
    private NewsReaderObjectType newsReaderObjectType;

    public int getEndPosition() {
        return endPosition;
    }

    public void setEndPosition(int endPosition) {
        this.endPosition = endPosition;
    }

    public int getStartPosition() {
        return startPosition;
    }

    public void setNewsReaderObjectType(NewsReaderObjectType newsReaderObjectType) {
        this.newsReaderObjectType = newsReaderObjectType;
    }

    public NewsReaderObjectType getNewsReaderObjectType() {
        return newsReaderObjectType;
    }

    public void setStartPosition(int startPosition) {
        this.startPosition = startPosition;
    }
}
