package com.paypal.domain;

public class Status {
    private String statusMessage;

    private Status(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public static Status.StatusBuilder builder() {
        return new Status.StatusBuilder();
    }

    public static class StatusBuilder {
        private String statusMessage;

        StatusBuilder() {
        }

        public Status.StatusBuilder statusMessage(String statusMessage) {
            this.statusMessage = statusMessage;
            return this;
        }

        public Status build() {
            return new Status(this.statusMessage);
        }

    }

}