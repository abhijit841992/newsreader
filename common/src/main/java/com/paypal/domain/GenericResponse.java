package com.paypal.domain;

public class GenericResponse<T> {
    private Status status;
    private T data;

    private GenericResponse(Status status, T data) {
        this.status = status;
        this.data = data;
    }

    public Status getStatus() {
        return status;
    }

    public T getData() {
        return data;
    }

    public static <T> GenericResponseBuilder builder() {
        return new GenericResponse.GenericResponseBuilder();
    }

    public static class GenericResponseBuilder<T> {
        private Status status;
        private T data;

        GenericResponseBuilder() {
        }

        public GenericResponse.GenericResponseBuilder status(Status status) {
            this.status = status;
            return this;
        }

        public GenericResponse.GenericResponseBuilder<T> data(T data) {
            this.data = data;
            return this;
        }

        public GenericResponse<T> build() {
            return new GenericResponse<T>(this.status, this.data);
        }

    }
}
