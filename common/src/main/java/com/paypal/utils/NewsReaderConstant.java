package com.paypal.utils;

public class NewsReaderConstant {

    public interface Formatter {
        String TWITTER_URL = "http://twitter.com/";
        String OPEN_A_HREF = "<a href=\"%s\">";
        String CLOSE_A_HREF = "</a>";
        String OPEN_STRONG_TAG = "<strong>";
        String CLOSE_STRONG_TAG = "</strong>";
    }
}

