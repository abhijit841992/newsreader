package com.paypal.enums;

public enum NewsReaderObjectType {
    ENTITY, LINK, USER_NAME;
}
